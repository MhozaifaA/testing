﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Storeak.Demo.Api.Infrastructure.DataModel;
using Storeak.Demo.Api.Infrastructure.DataModel.Main;
using Storeak.Demo.Api.Infrastructure.Mapping;
using StoreakApiService.Core.Context;

namespace Storeak.Demo.Api.Infrastructure
{
    public class DemoContext : StoreDbContext
    {
        public DemoContext(IHttpContextAccessor accessor)
            : base(accessor)
        {
        }

        public virtual DbSet<EmployeeDto> Employees { get; set; }
        public virtual DbSet<CategoryDto> Categories { get; set; }
        public virtual DbSet<OrderDto> Orders { get; set; }
        public virtual DbSet<ItemDto> Items { get; set; }
        public virtual DbSet<OrderItemDto> OrderItems { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new EmployeeMap());

            modelBuilder.Entity<EmployeeDto>().HasQueryFilter(x =>
               EF.Property<long>(x, "StoreId") == Client.StoreId);

            base.OnModelCreating(modelBuilder);
        }
    }
}
