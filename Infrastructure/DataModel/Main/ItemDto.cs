﻿using StoreakApiService.Core.Context.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Infrastructure.DataModel.Main
{
 
    [Table("Items")]
    public class ItemDto : TrackableDto
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }

        [Column( TypeName = "decimal(18,2)")]
        public decimal Buy { get; set; }
        [Column(TypeName = "decimal(18,2)")]
        public decimal Sell { get; set; }

        // every item should have only one Category 
        public Guid? CategoryId { get; set; } //nullable for un-classification yet
        //[ForeignKey("CategoryId")]
        public virtual CategoryDto Category { get; set; }

        //[ForeignKey("OrderItemId")]
        public virtual ICollection<OrderItemDto> OrderItems { get; set; }

    }
}
