﻿using StoreakApiService.Core.Context.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Infrastructure.DataModel.Main
{
    [Table("OrderItems")]
    public class OrderItemDto: TrackableDto
    {
        [DefaultValue(1)]
        public int Ammount { get; set; }
        public string Note { get; set; }
        [Column(TypeName ="decimal(18,2)")]
        public decimal TotalPrice { get; set; }

        public Guid OrderId { get; set; }
        //[ForeignKey("OrderId")]
        public virtual OrderDto Order { get; set; }

        public Guid ItemId { get; set; }
        //[ForeignKey("ItemId")]
        public virtual ItemDto Item { get; set; }
    }
}
