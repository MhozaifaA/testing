﻿using StoreakApiService.Core.Context.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Infrastructure.DataModel.Main
{
    [Table("Orders")]
    public class OrderDto : TrackableDto
    {
        public DateTime Date { get; set; } // order date
        public string Note { get; set; }
        public OrderStatus OrderStatus { get; set; }
        //[ForeignKey("ItemId")]
        public virtual ICollection<OrderItemDto> OrderItems { get; set; }


        /// <summary>
        /// A strange field only to store the customer. illogical
        /// </summary>
        public Guid ClientId { get; set; }
    }
}
