﻿using StoreakApiService.Core.Context.DataModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Infrastructure.DataModel.Main
{
    /// <summary>
    /// check trigger max deep level 3, with Expression 
    /// </summary>
    [Table("Categories")]
    public class CategoryDto: TrackableDto
    {
        [Required]
        public string Name { get; set; }

        public Guid? CategoryId { get; set; }
        //[ForeignKey("CategoryId")]
        public virtual CategoryDto Category { get; set; }
        public virtual ICollection<CategoryDto> Categories { get; set; }


        //[ForeignKey("ItemId")]
        public virtual ICollection<ItemDto> Items { get; set; }
    }
}
