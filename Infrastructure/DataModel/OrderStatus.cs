﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Infrastructure.DataModel
{
    public enum OrderStatus :uint
    {
        Ordered=0, // const orderby
        Canceled=1,
        Sold=2,
        Delivered =3,
    }
}
