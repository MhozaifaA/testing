﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Storeak.Demo.Api.Infrastructure.DataModel.Main;
using StoreakApiService.Core.Context.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Infrastructure.Mapping
{
    public class CategoryMap : TrackableMap<CategoryDto>
    {
        public override void Configure(EntityTypeBuilder<CategoryDto> builder)
        {
            builder.HasMany(c => c.Categories).WithOne(c => c.Category).HasForeignKey(c => c.CategoryId);
            base.Configure(builder);
            //builder.ToTable("Categories");
        }
    }
}
