﻿using StoreakApiService.Core.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Infrastructure
{
    public class ResponseMessages : IResponseMessages
    {
        IResponsesManager _responsesManager;

        public ResponseMessages(IResponsesManager responsesManager)
        {
            _responsesManager = responsesManager;
        }

        public CustomResponse EmployeeNotFound
        {
            get { return _responsesManager.GetResponce("EmployeeNotFound"); }
        }

        public CustomResponse EmployeeUpdatedSuccessfully
        {
            get { return _responsesManager.GetResponce("EmployeeUpdatedSuccessfully"); }
        }

        public CustomResponse EmployeeDeletedSuccessfully
        {
            get { return _responsesManager.GetResponce("EmployeeDeletedSuccessfully"); }
        }

        public CustomResponse GlobalInternalServerError()
        {
            return _responsesManager.GetResponce("InternalServerError");
        }


        #region Category
        public CustomResponse CategoryNotFound
        {
            get { return _responsesManager.GetResponce(nameof(CategoryNotFound)); }
        }

        public CustomResponse CategoryUpdatedSuccessfully
        {
            get { return _responsesManager.GetResponce(nameof(CategoryUpdatedSuccessfully)); }
        }

        public CustomResponse CategoryDeletedSuccessfully
        {
            get { return _responsesManager.GetResponce(nameof(CategoryDeletedSuccessfully)); }
        }

        public CustomResponse CategoryLimitedTree
        {
            get { return _responsesManager.GetResponce(nameof(CategoryLimitedTree)); }
        }

        #endregion


        #region Item

        public CustomResponse ItemNotFound
        {
            get { return _responsesManager.GetResponce(nameof(ItemNotFound)); }
        }

        public CustomResponse ItemUpdatedSuccessfully
        {
            get { return _responsesManager.GetResponce(nameof(ItemUpdatedSuccessfully)); }
        }

        public CustomResponse ItemDeletedSuccessfully
        {
            get { return _responsesManager.GetResponce(nameof(ItemDeletedSuccessfully)); }
        }

        #endregion

        #region Order
        public CustomResponse OrdreNotFound
        {
            get { return _responsesManager.GetResponce(nameof(OrdreNotFound)); }
        }

        public CustomResponse OrderEnterForbidden
        {
            get { return _responsesManager.GetResponce(nameof(OrderEnterForbidden)); }
        }

        public CustomResponse OrderUpdatedSuccessfully
        {
            get { return _responsesManager.GetResponce(nameof(OrderUpdatedSuccessfully)); }
        }

        public CustomResponse OrderDeletedSuccessfully
        {
            get { return _responsesManager.GetResponce(nameof(OrderDeletedSuccessfully)); }
        }
        #endregion

        #region OrderItem

        public CustomResponse OrderItemNotFound
        {
            get { return _responsesManager.GetResponce(nameof(OrderItemNotFound)); }
        }

        public CustomResponse OrderItemUpdatedSuccessfully
        {
            get { return _responsesManager.GetResponce(nameof(OrderItemUpdatedSuccessfully)); }
        }

        public CustomResponse OrderItemDeletedSuccessfully
        {
            get { return _responsesManager.GetResponce(nameof(OrderItemDeletedSuccessfully)); }
        }

        #endregion

    }
}
