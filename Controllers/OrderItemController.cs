﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Storeak.Demo.Api.Application.BusinessUseCases.OrderItem;
using Storeak.Demo.Api.Application.Queries.OrderItem;
using StoreakApiService.Core.Controllers;
using StoreakApiService.Core.Security;

namespace Storeak.Demo.Api.Controllers
{
    public class OrderItemController : StoreakController
    {
        private IOrderItemService IOrderItemService { get; }
        public OrderItemQueries OrderItemQueries { get;  }

        public OrderItemController(IOrderItemService iOrderItemService, OrderItemQueries orderItemQueries)
        {
            IOrderItemService = iOrderItemService;
            OrderItemQueries = orderItemQueries;
        }

        [Authorize]
        [ClaimRequirement(ClaimTypes.Role, "Customer")]
        [Route("api/v1/orderItem/{id}")]
        [HttpGet]
        public async Task<IActionResult> Get(Guid id)
        {
            return await OrderItemQueries.Get(id);
        }

        [Authorize]
        [ClaimRequirement(ClaimTypes.Role, "Customer")]
        [Route("api/v1/orderItem/{id}")]
        [HttpPut]
        public async Task<IActionResult> Update([FromRoute] Guid id, [FromQuery] int Amount)
        {
            return await IOrderItemService.Update(id, Amount);
        }

        [Authorize]
        [ClaimRequirement(ClaimTypes.Role, "Customer")]
        [Route("api/v1/orderItem/{id}")]
        [HttpPut]
        public async Task<IActionResult> Delete(Guid id)
        {
            return await IOrderItemService.Delete(id);
        }

    }
}
