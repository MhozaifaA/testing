﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Storeak.Demo.Api.Application.BusinessUseCases.Item;
using Storeak.Demo.Api.Application.Queries.Item;
using Storeak.Demo.Api.Models.Item;
using StoreakApiService.Core.Controllers;
using StoreakApiService.Core.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Controllers
{
    public class ItemController : StoreakController
    {
        private  IItemService IItemService { get; }
        private ItemQueries ItemQueries { get; }

        public ItemController(IItemService iItemService, ItemQueries itemQueries)
        {
            IItemService = iItemService;
            ItemQueries = itemQueries;
        }

        [Route("api/v1/items/paging")]
        [HttpGet]
        public async Task<IActionResult> GetBy([FromBody]ItemFilterModel request) // [FromBody] ItemModel,[FromQuery] PagingParams
        {
            return await ItemQueries.GetBy(request);
        }

        [Route("api/v1/items/{id}")]
        [HttpGet]
        public async Task<IActionResult> Get(Guid id)
        {
            return await ItemQueries.Get(id);
        }

        [Authorize]
        [ClaimRequirement(ClaimTypes.Role, "StoreAdmin")]
        [Route("api/v1/items")]
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] ItemModel request)
        {
            return await IItemService.Create(request);
        }


        [Authorize]
        [ClaimRequirement(ClaimTypes.Role, "StoreAdmin")]
        [Route("api/v1/items/{id}")]
        [HttpPut]
        public async Task<IActionResult> Update([FromRoute] Guid id, [FromBody] ItemModel request)
        {
            return await IItemService.Update(id, request);
        }


        [Authorize]
        [ClaimRequirement(ClaimTypes.Role, "StoreAdmin")]
        [Route("api/v1/items/{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id)
        {
            return await IItemService.Delete(id);
        }

    }
}
