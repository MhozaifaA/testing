﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Storeak.Demo.Api.Application.BusinessUseCases.Category;
using Storeak.Demo.Api.Application.Queries.Category;
using Storeak.Demo.Api.Infrastructure;
using Storeak.Demo.Api.Models.Category.Base;
using StoreakApiService.Core.Controllers;
using StoreakApiService.Core.Responses;
using StoreakApiService.Core.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Controllers
{
    public class CategoryController : StoreakController
    {
        private ICategoryService ICategoryService { get; }
        private CategoryQueries CategoryQueries { get; }


        public CategoryController(
            ICategoryService iCategoryService , CategoryQueries categoryQueries)
        {
            ICategoryService = iCategoryService;
            CategoryQueries = categoryQueries;
        }

        [Route("api/v1/categories")]
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            return await CategoryQueries.GetAll();
        }

        [Route("api/v1/categories/{id}")]
        [HttpGet]
        public async Task<IActionResult> Get(Guid id)
        {
            return await CategoryQueries.Get(id);
        }

        [Authorize]
        [ClaimRequirement(ClaimTypes.Role, "StoreAdmin")]
        [Route("api/v1/categories")]
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CategoryBaseModel request)
        {
            return await ICategoryService.Create(request);
        }


        [Authorize]
        [ClaimRequirement(ClaimTypes.Role, "StoreAdmin")]
        [Route("api/v1/categories/{id}")]
        [HttpPut]
        public async Task<IActionResult> Update([FromRoute]Guid id,[FromBody] CategoryBaseModel request)
        {
            return await ICategoryService.Update(id,request);
        }


        [Authorize]
        [ClaimRequirement(ClaimTypes.Role, "StoreAdmin")]
        [Route("api/v1/categories/{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id)
        {
            return await ICategoryService.Delete(id);
        }



    }
}
