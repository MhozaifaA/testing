﻿using Microsoft.AspNetCore.Mvc;
using Storeak.Demo.Api.Application.Queries.Report;
using StoreakApiService.Core.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Controllers
{
    public class ReportController : StoreakController
    {
        private ReportQueries ReportQueries { get; }

        public ReportController(ReportQueries ReportQueries)
        {
            this.ReportQueries = ReportQueries;
        }


        [Route("api/v1/Daily")]
        [HttpGet]
        public async Task<IActionResult> Daily(DateTime? from ,DateTime? to) // from query
        {
            return await ReportQueries.DailyStatisticsOrder(from,to);
        }

        [Route("api/v1/Sold")]
        [HttpGet]
        public async Task<IActionResult> Sold(DateTime from, DateTime to) // from query
        {
            return await ReportQueries.OrdersSold(from, to);
        }

    }
}
