﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Storeak.Demo.Api.Application.BusinessUseCases.Order;
using Storeak.Demo.Api.Application.Queries.Order;
using Storeak.Demo.Api.Models.Order;
using Storeak.Demo.Api.Models.Order.Base;
using StoreakApiService.Core.Controllers;
using StoreakApiService.Core.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Controllers
{
    public class OrderController :StoreakController
    {
        private IOrderService IOrderService { get; }
        private OrderQueries OrderQueries { get; }

        public OrderController(IOrderService iOrderService, OrderQueries orderQueries)
        {
            IOrderService = iOrderService;
            OrderQueries = orderQueries;
        }

        [Route("api/v1/orders/search")]
        [HttpGet]
        public async Task<IActionResult> Search([FromBody] OrderFilterModel request) 
        {
            return await OrderQueries.Search(request);
        }

        [Route("api/v1/orders/{id}")]
        [HttpGet]
        public async Task<IActionResult> Get(Guid id)
        {
            return await OrderQueries.Get(id);
        }

        [Authorize]
        [ClaimRequirement(ClaimTypes.Role, "Customer")]
        [Route("api/v1/orders")]
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] OrderModel request)
        {
            return await IOrderService.CreateOrUpdate(request);
        }


        [Authorize]
        [ClaimRequirement(ClaimTypes.Role, "Customer")]
        [Route("api/v1/orders/{id}")]
        [HttpPut]
        public async Task<IActionResult> Cansel([FromRoute] Guid id, [FromBody] OrderBaseModel request)
        {
            request.ClientId = this.ApplicationClient.AppId;
            return await IOrderService.Cancel(id, request);
        }


        [Authorize]
        [ClaimRequirement(ClaimTypes.Role, "Customer")]
        [Route("api/v1/orders/{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id)
        {
            return await IOrderService.Delete(id);
        }
    }
}
