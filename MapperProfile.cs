﻿using AutoMapper;
using Storeak.Demo.Api.Infrastructure.DataModel;
using Storeak.Demo.Api.Infrastructure.DataModel.Main;
using Storeak.Demo.Api.Models.Category;
using Storeak.Demo.Api.Models.Category.Base;
using Storeak.Demo.Api.Models.Item;
using Storeak.Models.Demo.BusinessUseCases.Employee;
using Storeak.Models.Demo.Queries.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Storeak.Demo.Api.Models.OrderItem;

namespace Storeak.Demo.Api
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<EmployeeDto, CreateEmployeeModel>().ReverseMap();
            CreateMap<EmployeeDto, GetAllEmployeeModel>().ReverseMap();
            CreateMap<EmployeeDto, GetEmployeeModel>().ReverseMap();

            // note: no need for more complex of mapping to allow flexibility edit and refactoring

            #region Category

            CreateMap<CategoryDto, CategoryBaseModel>().ReverseMap();
            CreateMap<CategoryDto, GetCategoryModel>().ReverseMap();

            #endregion

            #region Item
            CreateMap<ItemDto, ItemModel>().ReverseMap();
            #endregion

            #region OrderItem
            CreateMap<OrderItemDto, OrderItemModel>().ReverseMap();
            #endregion
        }
    }
}
