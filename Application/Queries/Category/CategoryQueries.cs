﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Storeak.Demo.Api.Infrastructure;
using Storeak.Demo.Api.Infrastructure.DataModel.Main;
using Storeak.Demo.Api.Models.Category;
using Storeak.Demo.Api.Models.Category.Base;
using StoreakApiService.Core.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Application.Queries.Category
{
    public class CategoryQueries
    {
        private readonly IMapper _mapper;
        private readonly ResponseMessages _responsMessages;
        private readonly UnitOfWork _unitOfWork;

        public CategoryQueries(IMapper mapper, UnitOfWork unitOfWork, IResponseMessages responsMessages)
        {
            _mapper = mapper;
            _responsMessages = responsMessages as ResponseMessages;
            _unitOfWork = unitOfWork;
        }

        public async Task<CustomResponse> GetAll()
        {
            var result = await _unitOfWork.CategoryRepository
                                    .GetAll().Select(category => new GetCategoryModel()
                                    {
                                        Id = category.Id,
                                        Name = category.Name,
                                        Categories = category.Categories.Select(c => new CategoryBaseModel()
                                        {
                                            Id = c.Id,
                                            Name = c.Name,
                                        }),
                                    }).ToListAsync();

            return new OkResponse(result);
        }


        public async Task<CustomResponse> Get(Guid id)
        {
            CategoryDto category = await _unitOfWork.CategoryRepository.FindAsync(id);
            if (category == null)
                return _responsMessages.CategoryNotFound;

            CategoryBaseModel model = _mapper.Map<CategoryBaseModel>(category);

            return new OkResponse(model);
        }

    }
}
