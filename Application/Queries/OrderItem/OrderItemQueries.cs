﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Storeak.Demo.Api.Application.Base;
using Storeak.Demo.Api.Infrastructure.DataModel.Main;
using Storeak.Demo.Api.Models.OrderItem;
using StoreakApiService.Core.Responses;

namespace Storeak.Demo.Api.Application.Queries.OrderItem
{
    public class OrderItemQueries
        : ServiceBase
    {
        public OrderItemQueries(IMapper mapper, UnitOfWork unitOfWork, IResponseMessages responsMessages) :
            base(mapper, unitOfWork, responsMessages)
        { }


        public async Task<CustomResponse> Get(Guid id)
        {
            OrderItemDto category = await _unitOfWork.Repository<OrderItemDto>().FindAsync(id);
            if (category == null)
                return _responsMessages.OrderItemNotFound;
            OrderItemModel model = _mapper.Map<OrderItemModel>(category);
            return new OkResponse(model);
        }

    }
}
