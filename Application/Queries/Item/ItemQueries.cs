﻿using AutoMapper;
using Storeak.Demo.Api.Infrastructure;
using Storeak.Demo.Api.Infrastructure.DataModel.Main;
using Storeak.Demo.Api.Models.Item;
using StoreakApiService.Core.Context;
using StoreakApiService.Core.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Application.Queries.Item
{
    public class ItemQueries
    {
        private readonly IMapper _mapper;
        private readonly ResponseMessages _responsMessages;
        private readonly UnitOfWork _unitOfWork;

        public ItemQueries(IMapper mapper, UnitOfWork unitOfWork, IResponseMessages responsMessages)
        {
            _mapper = mapper;
            _responsMessages = responsMessages as ResponseMessages;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Filter body and get pagination  
        /// </summary>
        /// <returns></returns>
        public async Task<CustomResponse> GetBy(ItemFilterModel item)
        {
            var result = await _unitOfWork.ItemRepository
                                    .GetAll()
                                     .Where(ItemFilter(item.MaskItemModel())) 
                                    .GetPagedAsync<ItemDto, ItemModel>(item.MaskPagingParams(), _mapper);

            return new OkResponse(result);
        }

        private Expression<Func<ItemDto, bool>> ItemFilter(ItemModel _item)
        {
            // search with Description by custom fuzzy string
            // you can add range filter  for buy/sell
            return item => item.Id == _item.Id || item.Name.Contains(_item.Name) ||
                           item.Buy == _item.Buy || item.Sell == _item.Sell ||
                           item.CategoryId == _item.Category.Id || item.Category.Name.Contains(_item.Category.Name);
        }

        public async Task<CustomResponse> Get(Guid id)
        {
            ItemDto item = await _unitOfWork.ItemRepository.FindAsync(id);
            if (item == null)
                return _responsMessages.ItemNotFound;

            ItemModel model = _mapper.Map<ItemModel>(item);
            return new OkResponse(model);
        }
    }
}
