﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Storeak.Demo.Api.Application.Base;
using Storeak.Demo.Api.Infrastructure.DataModel;
using Storeak.Demo.Api.Infrastructure.DataModel.Main;
using Storeak.Demo.Api.Models.Order;
using Storeak.Demo.Api.Models.Order.Base;
using Storeak.Demo.Api.Models.Report;
using StoreakApiService.Core.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Application.Queries.Report
{
    public class ReportQueries : ServiceBase
    {
        
        public ReportQueries(IMapper mapper, UnitOfWork unitOfWork, IResponseMessages responsMessages) :
            base(mapper, unitOfWork, responsMessages)
        { }


        /// <summary>
        ///  GroupBy Date then <see cref="OrderStatus"/> then collect them in range dates 
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public async Task<CustomResponse> DailyStatisticsOrder(DateTime? from=null,DateTime? to=null)
        {

            FixDate(ref from, ref to);
            // note GroupBy up 2.x  limited -- to enabled sure change it to client 
            var dictionary = await _unitOfWork.Repository<OrderDto>().GetAll().
                Where(order => order.CreatedDate >= from && order.CreatedDate <= to
                 && (order.OrderStatus == OrderStatus.Canceled || order.OrderStatus == OrderStatus.Ordered)).
                 OrderBy(order=>order.CreatedDate)
                .GroupBy(order => order.CreatedDate.Date)
                .ToDictionaryAsync(group=> group.Key, grou=> grou.Select(x=>new OrderBaseModel() {
                    Date= x.Date,
                    Id=x.Id,
                    Note=x.Note,
                    ClientId= x.ClientId,
                    OrderStatus= x.OrderStatus,
                }).ToList());

            int stepDays = to.Value.Subtract(from.Value).Days;

            var data= Enumerable.Range(0, stepDays + 1).Select(range=> new DailyStaticOrderModel
            {
                Date= from.Value.AddDays(range).Date,
                CanceledCount = dictionary.GetValueOrDefault(from.Value.Date.AddDays(range)).Where(x=>x.OrderStatus==OrderStatus.Canceled)?.Count()??0,
                OrderCount = dictionary.GetValueOrDefault(from.Value.Date.AddDays(range)).Where(x => x.OrderStatus == OrderStatus.Canceled)?.Count() ?? 0,
                Orders = dictionary.GetValueOrDefault(from.Value.Date.AddDays(range)),
            });

            /*
            result 
            [
              {
            "Date":from,
            "CanceledCount": number,
            "OrderCount": number,
            "Orders": [ { "Id": , "Date": ,"OrderStatus": Canseled or Order  }, {  },...     ]
            },
            {
              Date=from+day
            },
            ...
            ]
             */

            return new OkResponse(data);
        }

        private void FixDate(ref DateTime? from , ref DateTime? to)
        {
            if (!from.HasValue && !to.HasValue)
            {
                from = _unitOfWork.Repository<OrderDto>().GetAll().DefaultIfEmpty(null).Min(x => (DateTime?)x.CreatedDate) ?? null;
                to = _unitOfWork.Repository<OrderDto>().GetAll().DefaultIfEmpty(null).Max(x => (DateTime?)x.CreatedDate) ?? null;
            }
            else if (from.HasValue && !to.HasValue)
                to = _unitOfWork.Repository<OrderDto>().GetAll().DefaultIfEmpty(null).Max(x => (DateTime?)x.CreatedDate) ?? null;
            else if (!from.HasValue && to.HasValue)
                from = _unitOfWork.Repository<OrderDto>().GetAll().DefaultIfEmpty(null).Min(x => (DateTime?)x.CreatedDate) ?? null;
        }


        public async Task<CustomResponse> OrdersSold(DateTime from , DateTime to )
        {
            var data = await _unitOfWork.Repository<OrderDto>().GetAll().
             Where(order => order.CreatedDate >= from && order.CreatedDate <= to
              && (order.OrderStatus == OrderStatus.Sold || order.OrderStatus == OrderStatus.Delivered)).
              SelectMany(order => order.OrderItems.Select(x=>x.TotalPrice)).ToListAsync();
            return new OkResponse(new { Count = data.Count, Total = data.Sum(x => x) });
        }

    }
}
