﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Storeak.Demo.Api.Application.Base;
using Storeak.Demo.Api.Infrastructure.DataModel.Main;
using Storeak.Demo.Api.Models.Item;
using Storeak.Demo.Api.Models.Order;
using StoreakApiService.Core.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Application.Queries.Order
{
    public class OrderQueries : ServiceBase
    {
        public OrderQueries(IMapper mapper, UnitOfWork unitOfWork, IResponseMessages responsMessages) :
           base(mapper, unitOfWork, responsMessages)
        { }


        public async Task<CustomResponse> Search(OrderFilterModel order)
        {
            var result = await _unitOfWork.Repository<OrderDto>()
                                    .GetAll()
                                     .Where(OrderFilter(order)).ToListAsync();

            return new OkResponse(result);
        }

        private Expression<Func<OrderDto, bool>> OrderFilter(OrderFilterModel _order)
        {
            return order => order.Id == _order.Id || order.Note.Contains(_order.Note) ||
            order.Date == _order.Date || order.CreatedBy.Contains(_order.CreatedBy) ||
            order.CreatedDate == _order.CreatedDate;
        }


        public async Task<CustomResponse> Get(Guid id)
        {
            //OrderItemDto .where(orderId==id)
            OrderExtendedModel orderEx = await _unitOfWork.Repository<OrderDto>().GetAll()
                .Select(order => new OrderExtendedModel()
                {
                    Id=order.Id,
                    Date=order.Date,
                    Note=order.Note,
                    Items=order.OrderItems.Select(item=> new ItemModel() {
                    Id=item.Item.Id,
                    Buy=item.Item.Buy,
                    Description=item.Item.Description,
                    Name=item.Item.Name,
                    Sell=item.Item.Sell,
                    })
                }).FirstOrDefaultAsync();
            if (orderEx == null)
                return _responsMessages.OrdreNotFound;

            return new OkResponse(orderEx);
        }
    }
}
