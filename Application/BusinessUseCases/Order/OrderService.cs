﻿using AutoMapper;
using Storeak.Demo.Api.Application.Base;
using Storeak.Demo.Api.Infrastructure.DataModel;
using Storeak.Demo.Api.Infrastructure.DataModel.Main;
using Storeak.Demo.Api.Models.Order;
using Storeak.Demo.Api.Models.Order.Base;
using StoreakApiService.Core.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Application.BusinessUseCases.Order
{
    public class OrderService :ServiceBase, IOrderService
    {
        public OrderService(IMapper mapper, UnitOfWork unitOfWork, IResponseMessages responsMessages):
            base(mapper, unitOfWork, responsMessages) {}

        public async Task<CustomResponse> CreateOrUpdate(OrderModel request)
        {
            OrderDto order = await _unitOfWork.Repository<OrderDto>().FindAsync(request.Id);

            ICollection<OrderItemDto> items = request.Items.Select(item => new OrderItemDto()
            {
                ItemId = item.ItemId,
                Ammount = item.Ammount,
                TotalPrice= item.TotalPrice,
                Note=item.Note
            }).ToList();

            if (order is null) // create
            {
                order = new OrderDto() {
                    Date = request.Date, // date maybe not equal CreatedDate 
                    Note = request.Note,
                    OrderItems = items,
                };
                _unitOfWork.Repository<OrderDto>().Add(order); // Add Async should return ValueTask
                await _unitOfWork.SaveChangesAsync();
                return new OkResponse(order.Id);
            }

            //update
            order.Date =  request.Date;
            order.Note = request.Note;
            order.OrderItems = items; // reset all
            await _unitOfWork.SaveChangesAsync();
            return new OkResponse(order.Id); ;
        }

        public async Task<CustomResponse> Delete(Guid id)
        {
            OrderDto order = await _unitOfWork.Repository<OrderDto>().FindAsync(id);
            if (order == null)
                return _responsMessages.OrdreNotFound;

            _unitOfWork.Repository<OrderDto>().Remove(order);
            await _unitOfWork.SaveChangesAsync();
            return _responsMessages.OrderDeletedSuccessfully;
        }

        public async Task<CustomResponse> Cancel(Guid id, OrderBaseModel request)
        {

            OrderDto order = await _unitOfWork.Repository<OrderDto>().FindAsync(id);
            if (order == null)
                return _responsMessages.OrdreNotFound;

            if (!request.ClientId.Equals(order.ClientId))
                return _responsMessages.OrderEnterForbidden;

            order.OrderStatus = OrderStatus.Canceled;
            order.Note = request.Note;
            await _unitOfWork.SaveChangesAsync();
            return _responsMessages.OrderDeletedSuccessfully;
        }
    }
}
