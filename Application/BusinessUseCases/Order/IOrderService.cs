﻿using Storeak.Demo.Api.Models.Order;
using Storeak.Demo.Api.Models.Order.Base;
using StoreakApiService.Core.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Application.BusinessUseCases.Order
{
    public interface IOrderService
    {
        Task<CustomResponse> CreateOrUpdate(OrderModel request);
        Task<CustomResponse> Cancel(Guid id, OrderBaseModel request);
        Task<CustomResponse> Delete(Guid id);
    }
}
