﻿using StoreakApiService.Core.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Storeak.Demo.Api.Application.Base;
using Storeak.Demo.Api.Infrastructure.DataModel.Main;

namespace Storeak.Demo.Api.Application.BusinessUseCases.OrderItem
{
    public class OrderItemService :ServiceBase, IOrderItemService
    {
        public OrderItemService(IMapper mapper, UnitOfWork unitOfWork, IResponseMessages responsMessages) :
            base(mapper, unitOfWork, responsMessages)
        { }

        public async Task<CustomResponse> Delete(Guid id)
        {
            OrderItemDto orderItem = await _unitOfWork.Repository<OrderItemDto>().FindAsync(id);
            if (orderItem == null)
                return _responsMessages.OrderItemNotFound;

            _unitOfWork.Repository<OrderItemDto>().Remove(orderItem);
            await _unitOfWork.SaveChangesAsync();
            return _responsMessages.OrderItemDeletedSuccessfully;
        }

        public async Task<CustomResponse> Update(Guid id, int Ammount)
        {
            if (Ammount==0) // valid in view UI
                return await Delete(id);

            OrderItemDto orderItem = await _unitOfWork.Repository<OrderItemDto>().FindAsync(id);
            if (orderItem == null)
                return _responsMessages.OrderItemNotFound;
            orderItem.Ammount = Ammount;
            await _unitOfWork.SaveChangesAsync();
            return _responsMessages.OrderItemUpdatedSuccessfully;
        }
    }
}
