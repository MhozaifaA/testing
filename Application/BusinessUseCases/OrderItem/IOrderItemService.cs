﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StoreakApiService.Core.Responses;

namespace Storeak.Demo.Api.Application.BusinessUseCases.OrderItem
{
    public interface IOrderItemService
    {
        Task<CustomResponse> Update(Guid id, int Ammount);
        Task<CustomResponse> Delete(Guid id);
    }
}
