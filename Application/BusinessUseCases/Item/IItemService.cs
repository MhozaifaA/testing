﻿using Storeak.Demo.Api.Models.Item;
using StoreakApiService.Core.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Application.BusinessUseCases.Item
{
    public interface IItemService
    {
        Task<CustomResponse> Create(ItemModel request);
        Task<CustomResponse> Update(Guid id, ItemModel request);
        Task<CustomResponse> Delete(Guid id);
    }
}
