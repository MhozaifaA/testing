﻿using AutoMapper;
using Storeak.Demo.Api.Infrastructure;
using Storeak.Demo.Api.Infrastructure.DataModel.Main;
using Storeak.Demo.Api.Models.Item;
using StoreakApiService.Core.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Application.BusinessUseCases.Item
{
    public class ItemService : IItemService
    {
        private readonly IMapper _mapper;
        private ResponseMessages _responsMessages;
        private UnitOfWork _unitOfWork;
        public ItemService(IMapper mapper, UnitOfWork unitOfWork, IResponseMessages responsMessages)
        {
            _mapper = mapper;
            _responsMessages = responsMessages as ResponseMessages;
            _unitOfWork = unitOfWork;
        }

        public async Task<CustomResponse> Create(ItemModel request)
        {
            ItemDto item = _mapper.Map<ItemDto>(request);
            _unitOfWork.Repository<ItemDto>().Add(item);
            await _unitOfWork.SaveChangesAsync();
            return new OkResponse(item.Id);
        }

        public async Task<CustomResponse> Delete(Guid id)
        {
            ItemDto item = await _unitOfWork.Repository<ItemDto>().FindAsync(id);
            if (item == null)
                return _responsMessages.ItemNotFound;

            _unitOfWork.Repository<ItemDto>().Remove(item);
            await _unitOfWork.SaveChangesAsync();
            return _responsMessages.ItemDeletedSuccessfully;
        }

        public async Task<CustomResponse> Update(Guid id, ItemModel request)
        {
            ItemDto item = await _unitOfWork.Repository<ItemDto>().FindAsync(id);
            if (item == null)
                return _responsMessages.ItemNotFound;

            item.Name = request.Name;
            item.Description = request.Description;
            item.Buy = request.Buy;
            item.Sell = request.Sell;

            if (request.Category != null) //is not null
                item.CategoryId = request.Category.Id; //Update CategoryItem
            if (request.Category.Id == null)
                item.CategoryId = null; // cat from item 

            await _unitOfWork.SaveChangesAsync();
            return _responsMessages.ItemUpdatedSuccessfully;
        }
    }
}
