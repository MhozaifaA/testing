﻿using AutoMapper;
using Storeak.Demo.Api.Infrastructure;
using Storeak.Demo.Api.Infrastructure.DataModel;
using Storeak.Demo.Api.Infrastructure.DataModel.Main;
using Storeak.Demo.Api.Models.Category.Base;
using StoreakApiService.Core.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Storeak.Demo.Api.Application.BusinessUseCases.Category.Extension;

namespace Storeak.Demo.Api.Application.BusinessUseCases.Category
{
    public class CategoryService : ICategoryService
    {
        private readonly IMapper _mapper;
        private ResponseMessages _responsMessages;
        private UnitOfWork _unitOfWork;

        public CategoryService(IMapper mapper, UnitOfWork unitOfWork, IResponseMessages responsMessages)
        {
            _mapper = mapper;
            _responsMessages = responsMessages as ResponseMessages;
            _unitOfWork = unitOfWork;
        }

        public async Task<CustomResponse> Create(CategoryBaseModel request)
        {
            CategoryDto category = _mapper.Map<CategoryDto>(request);

            if (await CheckCategoryLevel(category.CategoryId))
            {
                 _unitOfWork.CategoryRepository.Add(category);
                 await _unitOfWork.SaveChangesAsync();
                return new OkResponse(category.Id);
            }
            return _responsMessages.CategoryLimitedTree;
        }

        public async Task<CustomResponse> Update(Guid id, CategoryBaseModel request)
        {
            CategoryDto category = await _unitOfWork.CategoryRepository.FindAsync(id);
            if (category == null)
                return _responsMessages.CategoryNotFound;

            category.Name = request.Name;
            category.CategoryId = request.CategoryId; // parent
            await _unitOfWork.SaveChangesAsync();
            return _responsMessages.CategoryUpdatedSuccessfully;
        }

        public async Task<CustomResponse> Delete(Guid id)
        {
            CategoryDto category = await _unitOfWork.CategoryRepository.FindAsync(id);
            if (category == null)
                return _responsMessages.CategoryNotFound;

            _unitOfWork.CategoryRepository.Remove(category);
            await _unitOfWork.SaveChangesAsync();
            return _responsMessages.CategoryDeletedSuccessfully;
        }


        // 1 null 
        // 2 1  
        // 3 2  
        // max-3 
        private async Task<bool> CheckCategoryLevel(Guid? parentId, int level = 3)
        {

            if (parentId is null)
                return true;
            int iter = 0;
            CategoryDto order= await _unitOfWork.CategoryRepository.FindAsync(parentId);

           // if(order.Parents().Count()== level)

            while (order?.CategoryId != null)
            {
                iter++;
                order = await _unitOfWork.CategoryRepository.FindAsync(order.CategoryId);
                if (iter == level)
                    return false;
            }
            return true;
        }                      

      


    }
}
