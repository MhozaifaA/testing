﻿using Storeak.Demo.Api.Models.Category.Base;
using StoreakApiService.Core.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Application.BusinessUseCases.Category
{
    public interface ICategoryService
    {
        Task<CustomResponse> Create(CategoryBaseModel request);
        Task<CustomResponse> Update(Guid id, CategoryBaseModel request);
        Task<CustomResponse> Delete(Guid id);
    }
}
