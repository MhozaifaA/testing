﻿using Storeak.Demo.Api.Infrastructure.DataModel.Main;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Application.BusinessUseCases.Category.Extension
{
    public static class CategoryExtension
    {
        private static  List<CategoryDto> GetParents(this CategoryDto categories)
        {
            List<CategoryDto> cats = new List<CategoryDto>();
            if (categories is null)
                return cats;
            cats.Add(categories);
            return GetParents(categories.Category);
        }

        public static IEnumerable<CategoryDto> Parents(this CategoryDto _cats)
        {
            if (_cats is null)
                yield return _cats;
            while (_cats != null)
            {
                yield return _cats;
                _cats = _cats.Category;
            }
        }

        public static IEnumerable<CategoryDto> Parents(this IQueryable<CategoryDto> _cats)
        {
            foreach (var cat in _cats)
            {
                yield return cat;
                var c = cat.Category;
                while (c != null)
                {
                    yield return c;
                    c = c.Category;
                }
            }
        }

    }
}
