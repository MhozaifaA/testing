﻿using Microsoft.EntityFrameworkCore;
using Storeak.Demo.Api.Infrastructure;
using Storeak.Demo.Api.Infrastructure.DataModel;
using Storeak.Demo.Api.Infrastructure.DataModel.Main;
using StoreakApiService.Core.Context;
using StoreakApiService.Core.Context.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Application
{
    public class UnitOfWork
    {
        private DemoContext _context;
        public UnitOfWork(DemoContext context)
        {
            _context = context;
        }

        

        public GenericRepository<EmployeeDto> EmployeeRepository
        {
            get
            {
                return new GenericRepository<EmployeeDto>(_context.Employees);
            }
        }
        public GenericRepository<T> Repository<T>() where T : BasicTrackableDto
            => new GenericRepository<T>(_context.Set<T>());


        public GenericRepository<CategoryDto> CategoryRepository
        {
            get => new GenericRepository<CategoryDto>(_context.Categories);
        }

        public GenericRepository<ItemDto> ItemRepository
        {
            get => new GenericRepository<ItemDto>(_context.Items);
        }



        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            //foreach (var entry in _context.ChangeTracker.Entries())
            //{
            //    switch (entry.State)
            //    {
            //        case EntityState.Deleted:
            //            entry.State = EntityState.Modified;
            //            entry.CurrentValues["IsDeleted"] = true;
            //            break;
            //    }
            //}
            await _context.SaveChangesAsync();
        }
    }

    public class CustomGenericRepository<T> : GenericRepository<T> where T:class
    {
        public CustomGenericRepository(DbSet<T> entities) : base(entities)
        { }

        public T Find(params object[] keyValues)
        {
            var one= this._entities.Find(keyValues);
            var IsDeleted = (bool)one.GetType().GetProperty("IsDeleted").GetValue(one);
            if (IsDeleted)
                return null;
            return  this._entities.Find(keyValues);
        }
        public async Task<T> FindAsync(params object[] keyValues)
        {
            var one = await this._entities.FindAsync(keyValues);
             var IsDeleted= (bool)one.GetType().GetProperty("IsDeleted").GetValue(one);
             if (IsDeleted)
                 return null;
            return await this._entities.FindAsync(keyValues);
        }

        public override void Remove(T entity)
        {
            //base.Remove(entity);
            entity.GetType().GetProperty("IsDeleted").SetValue(entity, true);
        }

     

    }

}
