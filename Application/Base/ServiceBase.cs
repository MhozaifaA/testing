﻿using AutoMapper;
using Storeak.Demo.Api.Infrastructure;
using StoreakApiService.Core.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Application.Base
{
    /// <summary>
    /// abstract for must inject services
    /// <para> with custome service use DI for service or one inject for all </para>
    /// </summary>
    public abstract class ServiceBase
    {
        protected readonly IMapper _mapper;
        protected readonly ResponseMessages _responsMessages;
        protected readonly UnitOfWork _unitOfWork;

        public ServiceBase(IMapper mapper, UnitOfWork unitOfWork, IResponseMessages responsMessages)
        {
            _mapper = mapper;
            _responsMessages = responsMessages as ResponseMessages;
            _unitOfWork = unitOfWork;
        }
    }
}
