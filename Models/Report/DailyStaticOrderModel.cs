﻿using Storeak.Demo.Api.Models.Order.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Models.Report
{
    public class DailyStaticOrderModel
    {
        public IEnumerable<OrderBaseModel> Orders { get; set; }
        public DateTime Date{ get; set; }
        public long CanceledCount { get; set; }
        public long OrderCount { get; set; }
    }
}
