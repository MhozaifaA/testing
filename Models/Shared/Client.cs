﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Models.Shared
{
    public abstract class Client
    {
        [JsonIgnore]
        public Guid ClientId;
    }
}
