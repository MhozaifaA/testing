﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Models.OrderItem
{
    public class OrderItemModel
    {
        public Guid ItemId { get; set; }
        public Guid OrderId { get; set; }
        public int Ammount { get; set; }
        public decimal TotalPrice { get; set; }
        public string Note { get; set; }
    }
}
