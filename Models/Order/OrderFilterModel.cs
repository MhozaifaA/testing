﻿using Storeak.Demo.Api.Models.Order.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Models.Order
{
    public class OrderFilterModel:OrderBaseModel
    {
        public DateTime CreatedDate { get; set; }
        public string CreatedBy{ get; set; }
    }
}
