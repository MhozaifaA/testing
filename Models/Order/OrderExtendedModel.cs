﻿using Storeak.Demo.Api.Models.Item;
using Storeak.Demo.Api.Models.Order.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Models.Order
{
    public class OrderExtendedModel :OrderBaseModel
    {
        public IEnumerable<ItemModel> Items{ get; set; }
    }
}
