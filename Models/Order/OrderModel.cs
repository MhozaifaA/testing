﻿using Storeak.Demo.Api.Models.Item;
using Storeak.Demo.Api.Models.Order.Base;
using Storeak.Demo.Api.Models.OrderItem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Models.Order
{
    public class OrderModel: OrderBaseModel
    {
     
        /// <summary>
        /// mostly contain Item Id only 
        /// </summary>
        public IEnumerable<OrderItemModel> Items { get; set; }
    }
}
