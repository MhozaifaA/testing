﻿using Storeak.Demo.Api.Infrastructure.DataModel;
using Storeak.Demo.Api.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Models.Order.Base
{
    public class OrderBaseModel:Client
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public string Note { get; set; }
        public OrderStatus  OrderStatus{ get; set; }
    }
}
