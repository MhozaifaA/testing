﻿using Storeak.Demo.Api.Models.Category.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Models.Item
{
    public class ItemModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Buy { get; set; }
        public decimal Sell { get; set; }
        public virtual CategoryBaseModel Category { get; set; }
    }
}
