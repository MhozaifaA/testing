﻿using Storeak.Demo.Api.Models.Category.Base;
using StoreakApiService.Core.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Models.Item
{
    // mixin with ItemModel 
    public class ItemFilterModel : PagingParams
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Buy { get; set; }
        public decimal Sell { get; set; }
        public virtual CategoryBaseModel Category { get; set; }


        // could not be readonly variable  for serializing 
        public PagingParams MaskPagingParams()
        {
            return this as PagingParams; 
        }

        public ItemModel MaskItemModel()
        { 
            return new ItemModel()
            {
              Id=Id,
              Name=Name,
              Description=Description,
              Buy=Buy,
              Sell=Sell,
              Category=Category,
            };
        }
    }
}
