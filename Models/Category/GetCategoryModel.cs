﻿using Storeak.Demo.Api.Models.Category.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Models.Category
{
    public class GetCategoryModel: CategoryBaseModel
    {
        /// <summary>
        /// children
        /// </summary>
        public IEnumerable<CategoryBaseModel> Categories{ get; set; }
    }
}
