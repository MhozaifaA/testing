﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Storeak.Demo.Api.Models.Category.Base
{
    /// <summary>
    /// abstract model
    /// </summary>
    public class CategoryBaseModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        /// <summary>
        /// Parent
        /// </summary>
        public Guid? CategoryId{ get; set; }
    }
}
